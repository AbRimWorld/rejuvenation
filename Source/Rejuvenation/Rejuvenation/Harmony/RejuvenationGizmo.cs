﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;
using Verse.AI;
using Verse.AI.Group;

namespace Rejuvenation
{

	[HarmonyPatch(typeof(Pawn), "GetGizmos")]
	public class RejuvenationPawn_GetGizmos
	{
		[HarmonyPostfix]
		public static void AddRejuGizmo(ref IEnumerable<Gizmo> __result, Pawn __instance)
		{
			Pawn pawn = __instance;
			List<Gizmo> gizmoList = __result.ToList();
			bool isPlayerOwnedPawn = pawn.Faction?.IsPlayer == true;
			//bool hasRejuvenationHediff = pawn.health?.hediffSet?.HasHediff(RejuHediffDefOf.Reju) == true;
			Hediff hediff = pawn.health?.hediffSet?.GetFirstHediffOfDef(RejuHediffDefOf.Reju_ArchoTechAgeController);//the Hediff that gives us the Gizmo

			if (isPlayerOwnedPawn && (hediff != null))
			{
				Gizmo rejuvenationGizmo = CreateRejuvenationGizmo(hediff);
				if (rejuvenationGizmo != null)
				{
					gizmoList.Add(rejuvenationGizmo);
				}
			}

			__result = gizmoList;
		}

		private static Gizmo CreateRejuvenationGizmo(Hediff hediff)
		{
			HediffComp_Rejuvenation_Targeted comp = hediff.TryGetComp<HediffComp_Rejuvenation_Targeted>();
			if (comp == null)
			{
				return null;
			} 
			Gizmo gizmo = new Command_Action
			{
				defaultLabel = "Reju_Gizmo_Label".Translate(),
				defaultDesc = "Reju_Gizmo_Description".Translate(),
				icon = ContentFinder<Texture2D>.Get(("UI/Icons/HediffGizmo/AgeSetter"), true),
				
				action = delegate
				{
					Action<string> saveAction = delegate (string s)
					{
						float age = float.Parse(s);
						comp.TargetAge = age;
					};
					Action discardAction = () => { };
					Window_CustomTextField window = new Window_CustomTextField(comp.TargetAge.ToString(), saveAction, discardAction);
					Find.WindowStack.Add(window);
				}

			};
			return gizmo;
		}
	}
}
