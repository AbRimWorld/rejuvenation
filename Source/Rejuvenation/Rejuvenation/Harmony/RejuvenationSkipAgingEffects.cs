﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;

namespace Rejuvenation
{
	[HarmonyPatch(typeof(Pawn_AgeTracker), "AgeTick")]
	public class RejuvenationSkipAgingEffects
	{
		[HarmonyPrefix]
		private static bool SkipAgingEffects(ref Pawn ___pawn)//, bool __result
		{
			try
			{
				Pawn pawn = ___pawn;
				List<Hediff> hediffs = pawn?.health?.hediffSet?.hediffs;
				if (hediffs.EnumerableNullOrEmpty())
				{
					return true;
				}
				IEnumerable<HediffComp_Rejuvenation> comps = hediffs
						.Select(hed => hed.TryGetComp<HediffComp_Rejuvenation>())
						.Where(hed => hed != null);
				// if we have no comps of the requested type, or no hediffs at all
				if (comps.EnumerableNullOrEmpty())
				{
					return true;
				}
				return false;
			}
			catch (Exception e)
			{
				Log.Warning("Rejuvenation: Something went wrong when trying to skip aging effects" + e);
				return true;
			}
			//return true;
		}
	}
}