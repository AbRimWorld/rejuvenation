﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Rejuvenation
{
	public abstract class HediffComp_Rejuvenation : HediffComp
	{
		int tickTracker = 0;

		public HediffCompProperties_Rejuvenation CompProp => (HediffCompProperties_Rejuvenation)props;
		public virtual bool CanChangeAge()
		{

			
			RLog.MessageOnce("Current Tick" + tickTracker);
			if (tickTracker >= CompProp.TickInterval)
			{
				RLog.MessageOnce("Can Change age via tick tracker.");
				tickTracker = 0;
				return true;
			}
			return false;
		}
		public abstract bool ShouldAgeDown();

		private void TickTracker()
		{
			tickTracker++;
		}

		public override void CompPostTick(ref float severityAdjustment)
		{
			base.CompPostTick(ref severityAdjustment);
			TickTracker();
			//base.Pawn.ageTracker.AgeBiologicalTicks -= 1;// called everytick, keeps from aging passivly
			if (CanChangeAge())
			{
				ApplyAgeChange();
			}
		}

		private void ApplyAgeChange()
		{
			int appliedAgeChange = CompProp.AgeChangeValue;
			if (ShouldAgeDown())//revert age// else increase
			{
				appliedAgeChange *= -1;
			}
			RLog.MessageOnce(" Before " + base.Pawn.ageTracker.AgeBiologicalTicks + " Pending value " + appliedAgeChange);
			base.Pawn.ageTracker.AgeBiologicalTicks += appliedAgeChange;
		}
	}
	public class HediffComp_Rejuvenation_Targeted : HediffComp_Rejuvenation
	{
		public float TargetAge;//to flaot
		
		public float PawnAge => base.Pawn.ageTracker.AgeBiologicalYearsFloat;//to flaot
		public override void CompPostMake()
		{
			base.CompPostMake();
			TargetAge = PawnAge;
		}
		public override void CompExposeData()
		{
			base.CompExposeData();
			Scribe_Values.Look(ref TargetAge, "TargetAge");//do not change this string when updating, if done it will reset for every user updating.
		}

		public override bool CanChangeAge()
		{
			if (!base.CanChangeAge())
			{
				return false;
			}
			if (TargetAge < 0)
			{
				RLog.MessageOnce(" Target age too low.");
				return false;
			}
			if (Math.Round(TargetAge, 2) == Math.Round(PawnAge, 2))//do nothing
			{
				RLog.MessageOnce(" Target age = pawn age.");
				return false;
			}
			return true;
		}

		public override bool ShouldAgeDown()
		{
			return TargetAge < PawnAge;
		}
	}
	public class HediffComp_Rejuvenation_Relative : HediffComp_Rejuvenation_Targeted
	{
		public HediffCompProperties_Rejuvenation_Relative Props => (HediffCompProperties_Rejuvenation_Relative)base.props;
		public override void CompPostMake()
		{
			base.CompPostMake();
			if (Props.ShouldAgeDecrease)
			{
				TargetAge -= Props.AgeDifference;
			}
			else
			{
				TargetAge += Props.AgeDifference;
			}
		}
	}
	public abstract class HediffCompProperties_Rejuvenation : HediffCompProperties
	{
		public int TickInterval = 60000;//60000
		public int AgeChangeValue = 120000;//120000
	}
	public class HediffCompProperties_Rejuvenation_Targeted : HediffCompProperties_Rejuvenation
	{
		public HediffCompProperties_Rejuvenation_Targeted()
		{
			base.compClass = typeof(HediffComp_Rejuvenation_Targeted);
		}
	}
	public class HediffCompProperties_Rejuvenation_Relative : HediffCompProperties_Rejuvenation
	{
		public float AgeDifference;//to float 
		public bool ShouldAgeDecrease = false;
		public HediffCompProperties_Rejuvenation_Relative()
		{
			base.compClass = typeof(HediffComp_Rejuvenation_Relative);
		}
	}
}
