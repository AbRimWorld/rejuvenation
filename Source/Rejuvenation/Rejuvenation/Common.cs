﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Rejuvenation
{
  [DefOf]
  public static class RejuHediffDefOf
  {
    static RejuHediffDefOf()
    {
      DefOfHelper.EnsureInitializedInCtor(typeof(RejuHediffDefOf));
    }
    //public static HediffDef Reju;//Dev Hediff ArchoTechAgeController
    public static HediffDef Reju_ArchoTechAgeController;
  }
}
