﻿using System;
using UnityEngine;
using Verse;

namespace Rejuvenation
{
  public class Window_CustomTextField : Window
  {
    public bool Saved = false;
    public bool Cancelled = false;
    public string TextFieldContent;
    private readonly Action saveAction;
    private readonly Action cancelAction;

    public Window_CustomTextField(string textFieldContent, Action<string> saveAction, Action cancelAction)
    {
      this.TextFieldContent = textFieldContent;
      this.saveAction = () => saveAction(TextFieldContent);
      this.cancelAction = cancelAction;
    }

    public void DoWindow()
    {
      absorbInputAroundWindow = true;//pressing off window does something
      focusWhenOpened = true;
      draggable = false;//can move it across the screen
      onlyOneOfTypeAllowed = true;//no duplicates
    }

    public override Vector2 InitialSize
    {
      get
      {
        return new Vector2(400, 99);
      }
    }

    public override void DoWindowContents(Rect inRect)
    {
      Rect exitButtonsRect = new Rect(inRect.x, inRect.y + inRect.height * 0.5f, inRect.width, inRect.height * 0.5f);
      if (DoSaveCancelButtons(exitButtonsRect, saveAction, cancelAction))
      {
        this.Close();
      }
      Rect fieldRect = new Rect(inRect.x, inRect.y, inRect.width, inRect.height - exitButtonsRect.height);
      TextFieldContent = DoLabelledTextField(fieldRect, "Reju_AgeWindow".Translate(), TextFieldContent);
    }
    public bool DoSaveCancelButtons(Rect inRect, Action saveAction, Action cancelAction)
    {
      string saveLabel = "Reju_Save".Translate();
      string buttonSpace = "    ";
      Vector2 saveButtonSize = Text.CalcSize(buttonSpace + saveLabel + buttonSpace);
      string cancelLabel = "Reju_Discard".Translate();
      Vector2 cancelButtonSize = Text.CalcSize(buttonSpace + cancelLabel + buttonSpace);
      float totalWidth = saveButtonSize.x + cancelButtonSize.x;
      float startX = inRect.width / 2 - totalWidth / 2;
      Rect saveRect = new Rect(startX, inRect.y, saveButtonSize.x, inRect.height);
      Rect cancelRect = new Rect(startX + saveButtonSize.x, inRect.y, cancelButtonSize.x, inRect.height);
      if (Widgets.ButtonText(saveRect, "Reju_Save".Translate()))
      {
        saveAction();
        return true;
      }
      if (Widgets.ButtonText(cancelRect, "Reju_Discard".Translate()))
      {
        cancelAction();
        return true;
      }
      return false;
    }
    public string DoLabelledTextField(Listing_Standard list, string label, string initialFieldContent, string labelTooltip = null, string textFieldTooltip = null)
    {
      Vector2 labelSize = Text.CalcSize(label);
      Rect rowRect = list.GetRect(labelSize.y);
      return DoLabelledTextField(rowRect, label, initialFieldContent, labelTooltip, textFieldTooltip);
    }

    public string DoLabelledTextField(Rect inRect, string label, string initialFieldContent, string labelTooltip = null, string textFieldTooltip = null)
    {
      Vector2 labelSize = Text.CalcSize(label);
      SplitRectVertically(inRect, out Rect labelRect, out Rect fieldRect, labelSize.x, -1, labelTooltip, textFieldTooltip, 15f);
      Widgets.Label(labelRect, label);
      return Widgets.TextField(fieldRect, initialFieldContent);
    }
    public void SplitRectVertically(Rect inRect, out Rect leftRect, out Rect rightRect, float leftWidth = -1f, float rightWidth = -1f, string leftTooltip = null, string rightTooltip = null, float paddingBetween = 0f)
    {
      bool isTooTight = leftWidth + rightWidth + paddingBetween > inRect.width;
      if (isTooTight)
      {
        // if the labels fit without padding, discard the padding
        if (leftWidth + rightWidth <= inRect.width)
        {
          paddingBetween = 0f;
        }
        // otherwise just trigger the calculation to split in the middle
        else
        {
          leftWidth = -1;
          rightWidth = -1;
        }
      }
      if (leftWidth == -1f && rightWidth == -1)
      {
        leftWidth = inRect.width / 2 - paddingBetween / 2;
        rightWidth = leftWidth;
      }
      else if (leftWidth == -1)
      {
        leftWidth = inRect.width - (rightWidth + paddingBetween);
      }
      else if (rightWidth == -1)
      {
        rightWidth = inRect.width - (leftWidth + paddingBetween);
      }
      leftRect = new Rect(inRect.x, inRect.y, leftWidth, inRect.height);
      float rightX = Math.Max(leftRect.width + paddingBetween, inRect.width - rightWidth);
      rightRect = new Rect(rightX, leftRect.y, rightWidth, leftRect.height);

      if (leftTooltip != null)
      {
        TooltipHandler.TipRegion(leftRect, leftTooltip);
      }
      if (rightTooltip != null)
      {
        TooltipHandler.TipRegion(rightRect, rightTooltip);
      }
    }
  }
}
