﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Rejuvenation
{
	public static class RLog
	{
		static List<string> sentMessages = new List<string>();
		public static void MessageOnce(string message)
		{
			if (!LoggingEnabled)
			{
				return;
			}
			if (sentMessages.Contains(message))
			{
				return;
			}
			Log.Message(message);
			sentMessages.Add(message);
		}
		public static bool LoggingEnabled = false;
	}
}
