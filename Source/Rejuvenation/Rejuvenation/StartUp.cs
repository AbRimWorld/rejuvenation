﻿using HarmonyLib;
using System.Reflection;
using Verse;

namespace Rejuvenation
{
  [StaticConstructorOnStartup]
  public static class Startup
  {
    static Startup()
    {
      Harmony harmony = new Harmony("Rejuvenation");
      harmony.PatchAll(Assembly.GetExecutingAssembly());
    }
  }
}
